# SPDX-FileCopyrightText: 2024 Scarlett Moore <sgmoore@kde.org>
#
# SPDX-License-Identifier: CC0-1.0
---
name: gwenview
confinement: strict
grade: stable
base: core24
adopt-info: gwenview
apps:
    gwenview:
        extensions:
        - kde-neon-6
        common-id: org.kde.gwenview.desktop
        desktop: usr/share/applications/org.kde.gwenview.desktop
        command: usr/bin/gwenview
        plugs:
        - home
        - cups-control
        - removable-media
        - udisks2
        - pulseaudio
        - mount-observe
environment:
  LD_LIBRARY_PATH: "/snap/kf6-core24/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:$SNAP/usr/lib:$SNAP/lib/:$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/pulseaudio:$SNAP/kf6/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/vlc:/usr/lib:/lib"
  VLC_PLUGIN_PATH: "/snap/kf6-core24/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/vlc/plugins"
layout:
  /usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/alsa-lib:
    bind: $SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/alsa-lib
slots:
    session-dbus-interface:
        interface: dbus
        name: org.kde.gwenview
        bus: session
package-repositories:
-   type: apt
    components:
    - main
    suites:
    - noble
    key-id: 444DABCF3667D0283F894EDDE6D4736255751E5D
    url: http://origin.archive.neon.kde.org/user
    key-server: keyserver.ubuntu.com
parts:
    kcolorpicker:
        plugin: cmake
        source: https://github.com/ksnip/kColorPicker.git
        source-tag: v0.3.1
        cmake-parameters:
        - -DCMAKE_INSTALL_PREFIX=/usr
        - -DCMAKE_BUILD_TYPE=Release
        - -DQT_MAJOR_VERSION=6
        - -DBUILD_WITH_QT6=ON
        - -DBUILD_TESTING=OFF
        - -DCMAKE_INSTALL_SYSCONFDIR=/etc
        - -DCMAKE_INSTALL_LOCALSTATEDIR=/var
        - -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF
        - -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_INSTALL_RUNSTATEDIR=/run
        - -DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON
        - -DCMAKE_VERBOSE_MAKEFILE=ON
        - -DCMAKE_INSTALL_LIBDIR=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - --log-level=STATUS
        - -DCMAKE_LIBRARY_PATH=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
    kimageannotator:
        after:
        - kcolorpicker
        plugin: cmake
        source: https://github.com/ksnip/kImageAnnotator.git
        source-tag: v0.7.1
        cmake-parameters:
        - -DCMAKE_INSTALL_PREFIX=/usr
        - -DCMAKE_BUILD_TYPE=Release
        - -DQT_MAJOR_VERSION=6
        - -DBUILD_WITH_QT6=ON
        - -DBUILD_TESTING=OFF
        - -DCMAKE_INSTALL_SYSCONFDIR=/etc
        - -DCMAKE_INSTALL_LOCALSTATEDIR=/var
        - -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF
        - -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_INSTALL_RUNSTATEDIR=/run
        - -DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON
        - -DCMAKE_VERBOSE_MAKEFILE=ON
        - -DCMAKE_INSTALL_LIBDIR=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - --log-level=STATUS
        - -DCMAKE_LIBRARY_PATH=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - "-DCMAKE_FIND_ROOT_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core24-sdk/current\\;/snap/kf6-core24-sdk/current/usr"
        - "-DCMAKE_PREFIX_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core24-sdk/current\\;/snap/kf6-core24-sdk/current/usr"
        build-environment: &buildenvironment
            - PATH: /snap/kde-qt6-core24-sdk/current/usr/bin${PATH:+:$PATH}
            - PKG_CONFIG_PATH: /snap/kde-qt6-core24-sdk/current/usr/lib/${CRAFT_ARCH_TRIPLET_BUILD_FOR}/pkgconfig${PKG_CONFIG_PATH:+:$PKG_CONFIG_PATH}
            - XDG_DATA_DIRS: $CRAFT_STAGE/usr/share:/snap/kde-qt6-core24-sdk/current/usr/share:/usr/share${XDG_DATA_DIRS:+:$XDG_DATA_DIRS}
            - XDG_CONFIG_HOME: $CRAFT_STAGE/etc/xdg:/snap/kde-qt6-core24-sdk/current/etc/xdg:/etc/xdg${XDG_CONFIG_HOME:+:$XDG_CONFIG_HOME}
            - LD_LIBRARY_PATH: "/snap/kde-qt6-core24-sdk/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/snap/kde-qt6-core24-sdk/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/libproxy:/snap/kde-qt6-core24-sdk/current/usr/lib:/snap/kf6-core24-sdk/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/snap/kf6-core24-sdk/current/usr/lib:$CRAFT_STAGE/usr/lib:$CRAFT_STAGE/lib/:$CRAFT_STAGE/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
    libkdcraw:
        plugin: cmake
        source: https://invent.kde.org/graphics/libkdcraw.git
        source-branch: release/24.12
        build-packages:
        - libraw-dev
        stage-packages:
        - libraw23t64
        cmake-parameters:
        - -DCMAKE_INSTALL_PREFIX=/usr
        - -DCMAKE_BUILD_TYPE=Release
        - -DQT_MAJOR_VERSION=6
        - -DBUILD_WITH_QT6=ON
        - -DBUILD_TESTING=OFF
        - -DCMAKE_INSTALL_SYSCONFDIR=/etc
        - -DCMAKE_INSTALL_LOCALSTATEDIR=/var
        - -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF
        - -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_INSTALL_RUNSTATEDIR=/run
        - -DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON
        - -DCMAKE_VERBOSE_MAKEFILE=ON
        - -DCMAKE_INSTALL_LIBDIR=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - --log-level=STATUS
        - -DCMAKE_LIBRARY_PATH=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        build-environment: *buildenvironment
    gwenview:
        after:
        - kimageannotator
        - libkdcraw
        parse-info:
        - usr/share/metainfo/org.kde.gwenview.appdata.xml
        plugin: cmake
        source: https://invent.kde.org/graphics/gwenview.git
        source-branch: release/24.12
        build-packages:
        - libwayland-dev
        - libxkbcommon-dev
        - wayland-protocols
        - libtiff-dev
        - libpng-dev
        - libexiv2-dev
        - libcfitsio-dev
        - libpulse0
        - libfreetype-dev
        build-snaps:
        - ffmpeg-2404-sdk
        stage-snaps:
        - ffmpeg-2404
        stage-packages:
        - libasound2
        - libasound2-plugins
        - libasound2-data
        - libwayland-client0
        - libxkbcommon0
        - wayland-protocols
        - libtiff6
        - libpng16-16
        - libexiv2-27
        - libcfitsio10t64
        - libpulse0
        - libfreetype6
        - libopenh264-7
        - cups-client
        cmake-parameters:
        - -DCMAKE_INSTALL_PREFIX=/usr
        - -DCMAKE_BUILD_TYPE=Release
        - -DQT_MAJOR_VERSION=6
        - -DBUILD_WITH_QT6=ON
        - -DBUILD_TESTING=OFF
        - -DCMAKE_INSTALL_SYSCONFDIR=/etc
        - -DCMAKE_INSTALL_LOCALSTATEDIR=/var
        - -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF
        - -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON
        - -DCMAKE_INSTALL_RUNSTATEDIR=/run
        - -DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON
        - -DCMAKE_VERBOSE_MAKEFILE=ON
        - -DCMAKE_INSTALL_LIBDIR=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - --log-level=STATUS
        - -DCMAKE_LIBRARY_PATH=lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - "-DCMAKE_FIND_ROOT_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core24-sdk/current\\;/snap/kf6-core24-sdk/current/usr"
        - "-DCMAKE_PREFIX_PATH=$CRAFT_STAGE\\;/snap/kde-qt6-core24-sdk/current\\;/snap/kf6-core24-sdk/current/usr"
        build-environment: *buildenvironment
        prime:
        - -usr/lib/*/cmake/*
        - -usr/include/*
        - -usr/share/ECM/*
        - -usr/share/man/*
        - -usr/share/icons/breeze-dark*
        - -usr/bin/X11
        - -usr/lib/gcc/$CRAFT_ARCH_TRIPLET_BUILD_FOR/6.0.0
        - -usr/lib/aspell/*
        - -usr/share/lintian
    gpu-2404:
        after: [gwenview]
        source: https://github.com/canonical/gpu-snap.git
        plugin: dump
        override-prime: |
            craftctl default
            ${CRAFT_PART_SRC}/bin/gpu-2404-cleanup mesa-2404
        prime:
        - bin/gpu-2404-wrapper
    cleanup:
        after:
        - gwenview
        plugin: nil
        build-snaps:
        - core24
        - kf6-core24
        override-prime: |
            set -eux
            for snap in "core24" "kf6-core24"; do
                cd "/snap/$snap/current" && find . -type f,l -exec rm -rf "${CRAFT_PRIME}/{}" \;
            done


